# softsys-git-hw

Welcome to Git! This homework is going to cover the basics of working with Git in a local and remote setting. This README.md will serve as your guide to complete the lab. Hopefully, everything will be outlined well enough to complete the lab, but feel free to email me at `jtk0018@auburn.edu` and ask questions if you get stuck. We will also go over this in our first lab so we can discuss there as well.

# GitLab Setup

Alright, so we first need to setup a GitLab account to use throughout the class. We are NOT using Auburn's GitLab so if you have that account setup you'll still need to make a normal GitLab one. If you have a regular GitLab account then congrats, you can skip this step! Let's get started!

### 1. Navigate to GitLab

Follow the link to [GitLab](https://gitlab.com/users/sign_in) and click register now.

![](/images/signUp.PNG)

### 2. Register

It is preferred you register with your Auburn email as the email and Auburn ID as your username if you only plan to use GitLab for the class. If you plan on using it outside of the class, feel free to use the email and username you prefer. Go through the remaining steps to setup your account. They'll ask you what you want to use it for so just answer as you see fit. For the question about team or personal use, select personal use.

### 3. Join Softsys-Students Group

Navigate to the menu in the top left of the page and select groups. Once you click groups, navigate to and click on explore groups.

![](/images/menu.PNG)

![](/images/groups.PNG)

In the Search by name bar, search for `Softsys-Students` and request access to the group. We will give you access as soon as we can. Now you will have access to any code we publish in the group and this will be where your repositories exist for the final project.

# Remote Repositories

## Getting Started

### 1. Git Installation

Let's start by making sure we have Git installed and setup on our VM. Perform the following commands in the terminal:
replace "your-name-here" with your actual name, surrounded with quotes. e.g. "Tanner Koza". Similarly, replace
"your-auburn-email-here" with your actual auburn email address surrounded with quotes. e.g. "jtk0018@auburn.edu"

```
sudo apt install git
git config --global user.name "your-name-here"
git config --global user.email "your-auburn-email-here"
```

So what did we just do? We installed Git locally on our machine and configured the name and email that will be associated with all of our commits.

### 2. Git Clone

Now that we have Git ready to go, let's clone this directory. `cd` to a directory of your choice and perform the following command:

```
git clone git@gitlab.com:softsys/softsys-students/softsys-git-hw.git
```

Wait! What just happened? Cloning issues? In order to clone using SSH, we need to generate an SSH key within our VM. If you didn't have issues, congrats! Your SSH key is already set up and referenced in GitLab. In that case, you should see the repository locally on your machine. Otherwise, refer to the [next section](#3-ssh-key-generation).

### 3. SSH Key Generation

So...what's a SSH key? Basically, it's an identifier associated with your computer that confirms your identity when cloning, pulling, and pushing to/from a remote repository host (i.e., GitLab, GitHub). Why does this matter? Basically, it makes things A LOT easier if you're continually interacting with remote repositories. You won't be prompted for your password for every interaction like you would be if you cloned using `https://` . Let's go ahead and set it up!

1. Generate your SSH key locally by performing the following commands: again, make sure to replace "your-auburn-email-here" with your email address surrounded by quotes.

```
cd
ssh-keygen -t ed25519 -C "your-auburn-email-here"
```

This creates your SSH key and should return the following:

```
> Generating public/private ed25519 key pair.
> Enter file in which to save the key (/home/<your-username>/.ssh/id_ed25519):
```

DO NOT enter a file name. Leave blank and press `ENTER` on your keyboard. This will save the SSH key in a default file. Then you will be prompted with the following:

```
> Enter passphrase (empty for no passphrase):
```

If desired, enter a passphrase. You can also leave this blank by pressing `ENTER` again. Then confirm your passphrase.
Congrats! You have created your SSH key, but now what?

2. Add SSH key to GitLab.

Now you need to copy your newly generated SSH key and paste it into your GitLab account's SSH key setup section in your GitLab preferences (top right cornder of the gitlab window, under the colorul circle). To copy, perform the following commands:

```
cd ~/.ssh
gedit id_ed25519.pub
```

This will open a gedit window with your key in it. Copy this key (excluding your email) and go into your GitLab preferences. The SSH Keys in your preferences can be found [here](https://gitlab.com/-/profile/keys). Once you get to your SSH keys setup, paste the SSH key in the textbox and save it with the name "Softsys VM". You should now be able to clone using the steps mentioned in the [Git Clone section](#2-git-clone).

![](/images/sshkey.PNG)

## Branching

### 1. Branch Fetching & Checkout

In this section, we'll explore some branching situations you're likely to run into. First, we'll pull a remote branch and create a local version of it. We currently have the remote branch `origin/main` locally as `main`. However, we want to pull the `origin/dev` branch to use as well. Perform the following commands to do this:

```
git fetch origin dev:dev
```

or to fetch and switch to `dev` use:

```
git checkout dev
```

The `dev:dev` part of the first command takes the remote branch `origin/dev` and assigns it a local name of `dev`, but doesn't check it out. Another way to do the same would be the following:

```
git fetch origin dev
git checkout origin/dev
git checkout -b dev
```

This way allows you to see what's happening a little clearer; however, the second command puts you in a detached HEAD state momentarily. Basically, you have checked out something that doesn't reference your local history and this can cause issues if you unknowingly make changes to this detached state. Therefore, if you prefer to use this method make sure you perform the third command to assign the detached HEAD state a local branch. Then you shouldn't have any issues. This same functionality can also be accomplished with the command [`git switch`](https://git-scm.com/docs/git-switch).

### 2. Branch Creation

Alright, let's explore another common branching situation. We're going to create our own branch locally based on `dev` and push it to the remote repository. Checkout the `dev` branch (MAKE SURE YOU DO THIS). Make sure by using the command `git branch`. Go ahead and perform the following command to make your own branch locally using the name `feature-<your-auburn-id>` (i.e., `feature-jtk0018`):

```
git checkout -b feature-<your-auburn-id>
```

Now you should have checked out your own branch locally. Let's add a change so we don't just push another version of `dev` to the remote repository. Perform the following commands to create a `.txt` file and write your name and student id in the text file.

```
touch me.txt
echo "<your-name> - <your-auburn-id>" > me.txt
```

Now we have changes to commit. Perform the following commands to do so:

```
git add me.txt
git commit -m "Add me.txt with my name and id"
```

Let's push our changes now. Take note that our new local branch doesn't exist in the remote repository; therefore, we have to add the `-u` flag to our push command to create a new remote branch based on our local branch. Perform the following command:

```
git push -u origin feature-<your-auburn-id>
```

Now you should be able to check the remote repository and see your branch and file are there.

![](/images/featureBranch.PNG)

![](/images/meTXT.PNG)

# Want To Get Ahead?

## Homework Setup

All homework for this class will be "submitted" using Git and GitLab. You will create your own private repository outside of the `Softsys-Students` group so no other students can access your work. Then you will share your repository with Dr. Chen and the two TA's. Let's walk through how to do that.

### 1. Create New Personal Repository

Navigate to the projects page from the menu we used to find our group before. Select the your projects link. On the new page, click New project in the upper-right hand corner.

![](/images/newProject.PNG)

Then, select create blank project. On the next page, you are able to create your repository. Go ahead and name it `hw-"your-auburn-id-here"` (eg. hw-jtk0018). Then make the namespace for your project URL your GitLab username. Leave the project slug alone and add any description you want. Finally, choose no deployment planned for the project deployment target and make the visibility level private and create the project.

![](/images/createRepo.PNG)

### 2. Add Dr. Chen and TAs to Repository

Now that your repository is created, let's add members. Click on the project information tab to the left and select members.

![](/images/membersTab.PNG)

From here, select the blue invite members button in the upper-right hand corner. Once selected, enter the following usernames in the username bar: `tannerkoza`, `carTloyal123`, and `howard0528`. Then under select a role, change the role to maintainers and click invite.

![](/images/addMembers.PNG)

Now we have access to your private repository so we can grade it without other people being able to see it.

### 3. Clone Repository to VM

Back on the project homepage, click on the blue clone button on the right to copy the SSH clone link. Then, in your VM, perform the following command in any directory you please to clone the repository:

```
cd <desired-directory>
git clone <insert-clone-link-here>
```

## Future Homework Turn In

So how will you turn in homework? Basically, we will be tracking the `main` branch of each of your homework repositories once you share them with us. This means that when it is time to turn in your work, we will simply pull your code off your `main` branch and test it. Therefore, it is IMPERATIVE to make your final commits to the `main` branch by midnight of the turn in date.

Immediately after each homework is turned in, create a new branch (eg. `hw1`) or tag (if you want to for fun) to save all of the work associated with that homework and then begin your next homework in the `main` branch. The homeworks mostly build upon each other so having everything in the main repository is okay.

Once again to reiterate, makes sure you have made your FINAL COMMITS to the `main` branch BY MIDNIGHT THE DAY HOMEWORK IS DUE. Feel free to ask any questions. We will also discuss this during our first Git lab.
